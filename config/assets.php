<?php
    
    return [
        'offline'        => env('ASSETS_OFFLINE', false),
        'enable_version' => env('ASSETS_ENABLE_VERSION', false),
        'version'        => env('ASSETS_VERSION', time()),
        'theme_folder'   => 'v1',
        'scripts'        => [
            'v1' => [
            
            ],
            'v2' => [
            
            ]
        ],
        'styles'         => [
            'v1' => [
    
            ],
            'v2' => [
    
            ]
        ],
        'resources'      => [
            'scripts' => [
                'v1' => [
    
                ],
                'v2' => [
    
                ]
            ],
            'styles'  => [
                'v1' => [
    
                ],
                'v2' => [
    
                ]
            ],
        ],
    ];
