<?php

    return [
        'offline' => env('ASSETS_OFFLINE', true),
        'enable_version' => env('ASSETS_ENABLE_VERSION', false),
        'version' => env('ASSETS_VERSION', time()),
        'scripts' => [
            'jquery',
        ],
        'styles' => [
            'bootstrap'
        ],
        'resources' => [
            'scripts' => [
                'jquery' => [
                    'use_cdn' => false,
                    'location' => 'footer',
                    'src' => [
                        'local' => 'app-assets/hostel/bower_components/jquery/js/jquery.min.js',
                    ]
                ]
            ],
            'styles' => [
                'bootstrap' => [
                    'use_cdn' => false,
                    'location' => 'header',
                    'src' => [
                        'local' => 'app-assets/bootstrap/css/bootstrap.min.css',
                    ],
                ]
            ],
        ],
    ];
